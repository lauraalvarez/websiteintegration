<?php
include('Infusionsoft/Infusionsoft/infusionsoft.php');

if (
    isset($_POST['firstname']) &&
    isset($_POST['surname']) &&
    isset($_POST['email']) &&
    isset($_POST['address1']) && 
    isset($_POST['address2']) &&
    isset($_POST['city']) &&
    isset($_POST['state']) &&
    isset($_POST['postalcode']) &&
    isset($_POST['country'])
) {
    echo json_encode($_POST);


    $contacts = Infusionsoft_DataService::query(new Infusionsoft_Contact(), array('Email' => $_POST['email']));

    if(count($contacts) > 0){
        $contact = array_shift($contacts);
    } else {
        $contact = new Infusionsoft_Contact();   
    }

    $contact->FirstName = $_POST['firstname'];
    $contact->LastName = $_POST['surname'];
    $contact->Email = $_POST['email'];
    $contact->Phone1 = $_POST['contactno'];
    $contact->StreetAddress1 = $_POST['address1'];
    $contact->StreetAddress2 = $_POST['address2'];
    $contact->City = $_POST['city'];
    $contact->State = $_POST['state'];
    $contact->PostalCode = $_POST['postalcode'];
    $contact->Country = $_POST['country'];
    $contact->EmailAddress2 = $_POST['alt_email'];

    $contact->save();
    
    $passedid = $contact->Id;
    $tagID = 220;
    $newtagapply = new Infusionsoft_ContactService();
    $newtagapply->addToGroup($passedid, $tagID);
    $newtagapply->save();

}
